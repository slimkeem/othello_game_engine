#include "engine.h"

namespace othello
{

bool OthelloGameEngine::initNewGame()
{
  return true;
}

void OthelloGameEngine::clearGame() {
}

bool OthelloGameEngine::performMoveForCurrentHuman(const BitPos& /*board_pos*/)
{
  return true;
}

void OthelloGameEngine::think(const std::chrono::seconds& /*time_limit*/)
{
}

PlayerId OthelloGameEngine::currentPlayerId() const
{
  return {};
}

PlayerType OthelloGameEngine::currentPlayerType() const
{
  return {};
}

BitPieces OthelloGameEngine::pieces(const PlayerId& /*player_id*/) const
{
  return {};
}

const BitBoard& OthelloGameEngine::board() const { static BitBoard bb; return bb; }


}   // namespace othello
