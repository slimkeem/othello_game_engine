#include "utility.h"

// stl
#include "numeric"
#include "iostream"

namespace othello::utility
{
  ////////////////////
  //
  //
  // Interface Utility
  // Functions
  //
  //
  ////////////////////

  BitPieces occupiedPositions(const BitBoard& /*board*/)
  {
    return {};
  }

  bool occupied(const BitPieces& /*pieces*/, const BitPos& /*board_pos*/)
  {
    return true;
  }

  bool occupied(const BitBoard& /*board*/, const BitPos& /*board_pos*/)
  {
    return false;
  }

  BitPos nextPosition(const BitPos& /*board_pos*/, const MoveDirection& /*dir*/)
  {
    return {};
  }

  BitPos findBracketingPiece(const BitBoard& /*board*/,
                             const BitPos& /*board_pos*/,
                             const PlayerId& /*player_id*/,
                             const MoveDirection& /*dir*/)
  {
    return {};
  }

  BitPosSet legalMoves(const BitBoard& /*board*/, const PlayerId& /*player_id*/)
  {
    return {};
  }

  bool isLegalMove(const BitBoard& /*board*/, const PlayerId& /*player_id*/,
                   const BitPos& /*board_pos*/)
  {

    return false;
  }


  void placeAndFlip(BitBoard& /*board*/, const PlayerId& /*player_id*/,
                    const BitPos& /*board_pos*/)
  {
  }

}   // namespace othello::utility
